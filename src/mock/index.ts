import { LocalGithubUser } from "../types";

export const defaultUser: LocalGithubUser = {
    "login": "ilya",
    "avatar": "https://loremflickr.com/200/200?random=1",
    "name": "Ilya Zabolotny",
    "company": "my company",
    "blog": "myblog.com",
    "location": "location",
    "bio": "React developer",
    "twitter": "username",
    "repos": 60,
    "followers": 184,
    "following": 5,
    "created": "2018-02-07T16:18:45Z",
}